* xref:index.adoc[Serveur de log Syslog]
** xref:index.adoc#généralités[Généralités]
*** xref:index.adoc#Les-catégories-de-messages[Les catégories de messages]
** xref:index.adoc#client-syslog[Client Syslog]
*** xref:index.adoc#la-commande-logwatch[La commande logwatch]
** xref:index.adoc#serveur-syslog[Serveur Syslog]
*** xref:index.adoc#stocker-les-logs-dans-des-fichiers-différenciés[Stocker les logs dans des fichiers différenciés]
** xref:index.adoc#stockage-en-base-de-données[Stockage en base de données]
