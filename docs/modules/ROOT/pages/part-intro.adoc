Les services sous LINUX.

Dans cette partie de notre support Formatux, nous vous proposons des supports pour les services les plus courants que vous pourriez être amenés à mettre en oeuvre :

* Bind, le service DNS,
* Samba, le service de partage de fichier compatible Windows,
* Apache, le serveur Web,
* Postfix, le service de messagerie,
* OpenLDAP, le serveur d'annuaire,
* etc.

Bonne lecture.
